const express = require('express');
const router = express.Router();
const GetPolizaService = require('../services/GetPolizaService');

/* GET home page. */
router.get('/', GetPolizaService.getPoliza);

module.exports = router;
