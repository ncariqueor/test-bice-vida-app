const https = require('https');

function getPoliza(req, res) {
    const url = 'https://dn8mlk7hdujby.cloudfront.net/interview/insurance/policy';

    https.request(url, response => {
        let str = '';
        let totalPoliza = 0;
        let copagoEmpleado = [];
        if (response.statusCode === 200) {
            response.on('data', chunk => {
                str += chunk;
            });
            response.on('end', () => {
                const policyResponse = JSON.parse(str);

                policyResponse.policy.workers.forEach(workers => {
                    workers.forEach(worker => {
                        let costoEmpleado = 0;
                        if (worker.age < 66) {
                            switch (worker.childs) {
                                case 0:
                                    totalPoliza += 0.279;
                                    costoEmpleado += 0.279;
                                    if (policyResponse.policy.has_dental_care) {
                                        totalPoliza += 0.12;
                                        costoEmpleado += 0.12;
                                    }
                                    copagoEmpleado.push({age: worker.age, childs: worker.childs, copago: parseFloat((costoEmpleado * 0.1).toFixed(4))});
                                    break;

                                case 1:
                                    totalPoliza += 0.4396;
                                    costoEmpleado += 0.4396;
                                    if (policyResponse.policy.has_dental_care) {
                                        totalPoliza += 0.1950
                                        costoEmpleado += 0.1950
                                    }
                                    copagoEmpleado.push({age: worker.age, childs: worker.childs, copago: parseFloat((costoEmpleado * 0.1).toFixed(4))});
                                    break;

                                default:
                                    totalPoliza += 0.5599;
                                    costoEmpleado += 0.5599;
                                    if (policyResponse.policy.has_dental_care) {
                                        totalPoliza += 0.2480
                                        costoEmpleado += 0.2480
                                    }
                                    copagoEmpleado.push({age: worker.age, childs: worker.childs, copago: parseFloat((costoEmpleado * 0.1).toFixed(4))});
                                    break;
                            }
                        }
                    });
                });

                const result = {
                    costoTotalPoliza: parseFloat(totalPoliza.toFixed(4)),
                    costoTotalPolizaEmpresa: parseFloat((totalPoliza * 0.9).toFixed(4)),
                    copagoPorEmpleado: copagoEmpleado

                };

                res.status(200).send(result);
            });
        } else {
            res.status(500).send({message: 'Error al obtener la póliza'});
        }
    }).end();
}

module.exports = {
  getPoliza
};
